#############################################################
## JOB DESCRIPTION                                         ##
#############################################################

# SYNTAX
# namd2 <options> --tclmain path/to/file.conf path/to/molecule restart ns dx dy dz mode
# WARNING!
# path/to/molecule > must be from G03-recoverin-mutation folder
# NO .pdb extension must be indicated
# the script can be launched from ANY subfolder of G03-recoverin-mutation
# restart > is the number of times you have restarted the simulation
# ns > is the number of ns you want to simulate
# dx,dy,dz > is the box dimension
# mode > 0 is NVE, 1 is NVT, 2 is NPT
#
# OUTPUT
# simulation of path/to/molecule in explicit solvent, various options
# dcd file saved in path/to/molecule_simul_<restart>.dcd

# MAIN PARAMETERS
set top_directory "G03-src/external/charmm_toppar_namd"; # topology directory

# go to the "G03-recoverin-mutation" directory
set directory [pwd] ; list
set directory_list [split $directory {/}] ; list
set main_directory "" ; list
foreach folder $directory_list {
	if {$folder == "G03-recoverin-mutation"} {
    append main_directory "G03-recoverin-mutation"
    break;
  }
	append main_directory $folder
	append main_directory "/"
}
cd $main_directory

#############################################################
## ADJUSTABLE PARAMETERS                                   ##
#############################################################

set name    [lindex $argv 0]
set restart [lindex $argv 1]
set output  [join [list $name "_simul_" $restart] ""]
set ns      [lindex $argv 2]
set dx      [lindex $argv 3]
set dy      [lindex $argv 4]
set dz      [lindex $argv 5]
set mode    [lindex $argv 6]

structure   $name.psf
coordinates $name.pdb
outputName  $output

set temperature 310

set timestep      2 ; # fs of each time step
set steps [expr int($ns*1000000/$timestep)] ; # n of required steps

set minim     10000 ; # n of energy minimization steps if restart is 0

if {$restart == 0} {
temperature $temperature ; # first time: assign velocities
firsttimestep      0
} else {
set previous [join [list $name "_simul_" [expr $restart-1] ] "" ] ; # name of previous simulation
binCoordinates     $previous.restart.coor
binVelocities      $previous.restart.vel  ;# remove the "temperature" entry if you use this!
extendedSystem	   $previous.restart.xsc
firsttimestep      [expr $restart*$steps] ;# restart from the right number of steps (assumed to be constant)
}

#############################################################
## SIMULATION PARAMETERS                                   ##
#############################################################

paraTypeCharmm	    on
parameters          $top_directory/par_all36_prot.prm
parameters          $top_directory/par_all36_lipid.prm
parameters          $top_directory/par_all36_na.prm
parameters          $top_directory/par_all36_carb.prm
parameters          $top_directory/par_all36_cgenff.prm
parameters          $top_directory/stream/lipid/toppar_all36_lipid_prot.str
parameters          $top_directory/stream/lipid/toppar_all36_lipid_sphingo_namd.str
parameters          $top_directory/toppar_water_ions_namd.str

# Periodic Boundary conditions: take the output of solvate.tcl script
if {$restart == 0} {
cellBasisVector1    $dx   0    0
cellBasisVector2     0   $dy   0
cellBasisVector3     0    0   $dz
cellOrigin           0    0    0
}
wrapWater           on
wrapAll             on


# Force-Field Parameters
exclude             scaled1-4
1-4scaling          1.0
cutoff              12.0
switching           on
switchdist          10.0
pairlistdist        14.0


# Integrator Parameters
timestep            2.0  ;# 2fs/step
rigidBonds          all  ;# needed for 2fs steps
nonbondedFreq       1
fullElectFrequency  2  
stepspercycle       10


#PME (for full-system periodic electrostatics)
if {0} {
PME                 yes
PMEGridSpacing      1.0

#manual grid definition
#PMEGridSizeX        32
#PMEGridSizeY        32
#PMEGridSizeZ        64
}


# Constant Temperature Control
if {$mode>=1} {
langevin            on    ;# do langevin dynamics
langevinDamping     1     ;# damping coefficient (gamma) of 5/ps
langevinTemp        $temperature
langevinHydrogen    no    ;# don't couple langevin bath to hydrogens
}


# Constant Pressure Control (variable volume)
if {$mode==2} {
useGroupPressure      yes ;# needed for 2fs steps
useFlexibleCell       no  ;# no for water box, yes for membrane
useConstantArea       no  ;# no for water box, yes for membrane

langevinPiston        on
langevinPistonTarget  1.01325 ;#  in bar -> 1 atm
langevinPistonPeriod  100.0
langevinPistonDecay   50.0
langevinPistonTemp    $temperature
}


restartfreq         [expr min(500,int($steps/100))]     ;# 10 times during simulation
dcdfreq             500
xstFreq             500
outputEnergies      500
outputPressure      500

# Fixed Atoms Constraint (set PDB beta-column to 1)
if {0} {
fixedAtoms          on
fixedAtomsFile      myfixedatoms.pdb
fixedAtomsCol       B
}


# IMD Settings (can view sim in VMD)
if {0} {
IMDon           on
IMDport         3000    ;# port number (enter it in VMD)
IMDfreq         1       ;# send every 1 frame
IMDwait         no      ;# wait for VMD to connect before running?
}


#############################################################
## EXTRA PARAMETERS                                        ##
#############################################################

# Put here any custom parameters that are specific to 
# this job (e.g., SMD, TclForces, etc...)



#############################################################
## EXECUTION SCRIPT                                        ##
#############################################################

# Minimization
if {$restart==0} {
minimize            $minim
reinitvels          $temperature
}

# run enough steps to make $ns nanoseconds
run $steps
